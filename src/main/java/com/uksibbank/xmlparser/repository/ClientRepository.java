package com.uksibbank.xmlparser.repository;

import com.uksibbank.xmlparser.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClientRepository extends JpaRepository<Client, Long> {
    Client getByInn(final String inn);
}
